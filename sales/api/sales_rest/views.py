from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from .models import (
    AutomobileVO,
    Customer,
    SaleRecord,
    SalesPerson
)

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
        "id"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]

class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "price",
        "sales_person",
        "customer",
        "automobile",
        "id",
    ]
    encoders={
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile":AutomobileVOEncoder(),
    }

# fetch auto VO >
@require_http_methods(["GET"])
def api_automobileVO(request):
    if request.method == "GET":
        automobileVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobile":automobileVO},
            encoder=AutomobileVOEncoder,
            safe=False
        )

# GET / POST Customer >
@require_http_methods(["GET", "POST"])
def api_customers_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message":"Could not create customer"}
            )
            response.status_code = 400
            return response

#  GET / DELETE Customer Details >
@require_http_methods(["GET","DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message":"invalid customer"}
            )
            response.status_code = 400
            return response
    else:
        try:
            request.method == "DELETE"
            count, _ = Customer.objects.filter(id=pk).delete()
            return JsonResponse(
                {"Deleted": count > 0}
            )
        except:
            response = JsonResponse(
                {"message":"invalid customer"}
            )
            response.status_code = 400
            return response

#  GET / POST employees plurrral >
@require_http_methods(["GET", "POST"])
def api_sales_people_list(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            sales_people = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_people,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create salesman"}
            )
            response.status_code = 400
            return response

# "GET","DELETE" to get individual salesman >
@require_http_methods(["GET","DELETE"])
def api_sales_person(request,pk):
    if request.method == "GET":
        try:
            salesman = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                {"salesman":salesman},
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message":"invalid salesman"}
            )
            response.status_code = 400
            return response


# GET / POST salerecord aka list of sales >
@require_http_methods(["GET","POST"])
def api_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records":sale_records},
            encoder=SaleRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            # customer
            customer = Customer.objects.get(id=content['customer'])
            content["customer"] = customer

            # automobile
            automobile = AutomobileVO.objects.get(id=content["automobile"])
            content["automobile"] = automobile

            # salesman
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person

            sale_records = SaleRecord.objects.create(**content)
            return JsonResponse(
                sale_records,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "unable create sale record... sowwy"}
            )
            response.status_code = 400
            return response

# Sale detail >
@require_http_methods(["GET", "DELETE"])
def api_sale_record(request, id):
    if request.method == "GET":
        try:
            sale_record = SaleRecord.objects.get(id=id)
            return JsonResponse(
                sale_record,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message":"invalid sales record hehehe"}
            )
            response.status_code = 400
            return response
    else:
        try:
            request.method == "DELETE"
            count, _ = SaleRecord.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted":count > 0}
            )
        except:
            response = JsonResponse(
                {"message":"NOT A RECORD BAHAHA"}
            )
            response.status_code = 400
            return response
