from django.shortcuts import render
from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder



class AutomobileVODetailEncoder(ModelEncoder):
    model=AutomobileVO
    properties =[
        'import_href',
        'vin',
        'id',
    ]

class TechnicianListEncoder(ModelEncoder):
    model=Technician
    properties=[
        'name',
        'employee_number',
        'id'
    ]

class TechnicianDetailEncoder(ModelEncoder):
    model= Technician
    properties = [
        'name',
        'employee_number',
        'id',
    ]


class AppointmentListEncoder(ModelEncoder):
    model=Appointment
    properties=[
        'vin',
        'customer_name',
        'datetime',
        'reason',
        'finished',
        "technician",
        "vip",
        'id',
    ]
    encoders={
        "technician":TechnicianDetailEncoder()
    }

class AppointmentDetailEncoder(ModelEncoder):
    model=Appointment
    properties=[
        'vin',
        'customer_name',
        'datetime',
        'reason',
        'vip',
        'finished',
        'technician',
        "id",
    ]
    encoders={
        'technician':TechnicianDetailEncoder()
    }




@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians= Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content=json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_technician(request, id):
    if request.method =="GET":
        technicians = Technician.objects.get(id=id)
        return JsonResponse(
            {"technicians":technicians},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count,_= Technician.objects.filter(id=id).delete()
        return JsonResponse ({"deleted": count>0})
    else:
        content= json.loads(request.body)
        try:
            technicians= Technician.objects.get(id=id)
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "They dont work here"},
                status=400,
            )
        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request,vin_vo=None):
    if request.method=="GET":
        if vin_vo is not None:
            appointments = Appointment.objects.filter(vin=vin_vo)
        else:
            appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder= AppointmentListEncoder,
            safe=False
        )
    else:
        content=json.loads(request.body)
        try:
            tech_id = content["technician"]
            tech= Technician.objects.get(id=tech_id)
            content["technician"]= tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Tech"}
            )
        try:
            vin=AutomobileVO.objects.get(vin=content["vin"])
            if vin is not None:
                content["vip"]=True
                appointment= Appointment.objects.create(**content)
                return JsonResponse(
                    {"appointment":appointment},
                    encoder=AppointmentDetailEncoder,
                    safe=False,
                )
        except AutomobileVO.DoesNotExist:
            appointment= Appointment.objects.create(**content)
            return JsonResponse(
                {"appointment":appointment},
                encoder=AppointmentDetailEncoder,
                safe=False,
            )



@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointments(request,id):
    if request.method=="GET":
        try:
            appointment =Appointment.objects.filter(id=id)
            return JsonResponse(
                {"appointment" :appointment},
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response=JsonResponse({"message":"Appointment does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            count,_=Appointment.objects.filter(id=id).delete()
            return JsonResponse({"deleted":count>0})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:
        try:
            content = json.loads(request.body)
            appointment= Appointment.objects.get(id=id)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status = 400
            )
        Appointment.objects.filter(id=id).update(**content)
        appointments= Appointment.objects.get(id=id)
        return JsonResponse(
            {"appointments":appointments},
            encoder= AppointmentDetailEncoder,
            safe= False
        )


@require_http_methods(["GET"])
def api_show_history(request, vin_vo=None):
    if request.method=="GET":
        if vin_vo is None:
            return JsonResponse(
                {"message": "Not found"},
                status=400,
            )
        else:
            appointment=Appointment.objects.get(vin=vin_vo)
        return JsonResponse(
            {"appointment":appointment},
            encoder=AppointmentDetailEncoder,
            safe=False
        )
# Create your views here.
