import React, { useEffect, useState } from "react";

function SaleRecordForm() {
  // customer with list of customes
  const [customers, setCustomers] = useState([]);
  const [customer, setCustomer] = useState("");
  // automobile with list of automobiles
  const [automobiles, setAutomobiles] = useState([]);
  const [automobile, setAutomobile] = useState("");
  // sales_person with list of salesman
  const [salesPeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState("");
  //price
  const [price, setPrice] = useState("");

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };

  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    handleCustomerChange(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.price = price;
    data.automobile = automobile;
    data.salesPerson = salesPerson;
    data.customer = customer;

    const salesRecordUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(salesRecordUrl, fetchConfig);
    if (response.ok) {
      const newSalesPerson = await response.json();

      setPrice("");
      setAutomobile("");
      setSalesPerson("");
      setCustomer("");
    }
  };
  const fetchData = async () => {
    const automobileUrl = "http://localhost:8090/api/automobiles/";
    const automobileResponse = await fetch(automobileUrl);
    if (automobileResponse.ok) {
      const automobileData = await automobileResponse.json();
      setAutomobiles(automobileData.automobileVO);
    }

    const salesPersonUrl = "http://localhost:8090/api/employees/";
    const salesPersonResponse = await fetch(salesPersonUrl);
    if (salesPersonResponse.ok) {
      const salesPersonData = await salesPersonResponse.json();
      setSalesPeople(salesPersonData.sales_person);
    }

    const customerUrl = "http://localhost:8090/api/customers/";
    const customerResponse = await fetch(customerUrl);
    if (customerResponse.ok) {
      const customerData = await customerResponse.json();
      setCustomer(customerData.customer);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a SaleRecord</h1>
          <form onSubmit={handleSubmit} id="create-SaleRecord-form">
            <div className="form-floating mb-3">
              <select
                onChange={handleCustomerChange}
                value={customer}
                placeholder="Customer"
                required
                name="customer"
                id="customer"
                className="form-control"
              >
                <option value="">Select A Customer</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.id} value={customer.name}>
                      {customer.name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="mb-3">
              <select
                value={automobile}
                onChange={handleAutomobileChange}
                required
                id="automobile"
                name="automobile"
                className="form-select"
              >
                something wrong here
                <option value="">Select Automobile</option>
                {automobiles?.map((automobile) => {
                  return (
                    <option key={automobile.id} value={automobile.model}>
                      {automobile.model}
                    </option>
                  );
                })}
              </select>
            </div>
            {/* make frop down for autos */}
            <div className="form-floating mb-3">
              <input
                onChange={handlePriceChange}
                value={price}
                placeholder="Price"
                type="text"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleSalesPersonChange}
                value={salesPerson}
                placeholder="salesperson"
                required
                name="salesperson"
                id="salesperson"
                className="form-control"
              >
                <option value="">SalesPerson</option>
              </select>
              {salesPeople?.map((salesPerson) => {
                return (
                  <option value={salesPerson.id} key={salesPerson.id}>
                    {salesPerson.name}
                  </option>
                );
              })}
              <label htmlFor="salesperson">SalesPerson</label>
            </div>
            <button className="btn btn-primary">Create A SaleRecord</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleRecordForm;

// dont forget to use map method thingy
