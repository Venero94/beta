import { NavLink, Link } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointment">
                Appointment
              </NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/technician/new/">
                Technician
              </NavLink>
            </li>

            <li className="nav-item dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Car Form / Car List
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="manufacturers">
                    Manufacturers
                  </Link>
                  <Link className="dropdown-item" to="manufacturers/new">
                    Add Manufacturers
                  </Link>
                  <Link className="dropdown-item" to="vehicle">
                    Models
                  </Link>
                  <Link className="dropdown-item" to="vehicle/new">
                    Add Models
                  </Link>
                  <Link className="dropdown-item" to="automobiles">
                    Automobiles
                  </Link>
                  <Link className="dropdown-item" to="automobiles/new">
                    Add Automobiles
                  </Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <button
                className="btn btn-success dropdown-toggle"
                type="button"
                id="dropdown"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </button>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="sales">
                    List of Sales
                  </Link>
                  <Link className="dropdown-item" to="sales/new">
                    Record A Sale
                  </Link>
                  <Link className="dropdown-item" to="employees">
                    Add Sales Person
                  </Link>
                  <Link className="dropdown-item" to="customer">
                    Add Potential Customer
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointment/history/">
                History
              </NavLink>
            </li>
            <li>
              {/* <NavLink className="nav-link" to="/vehicles">
                Vehicle
              </NavLink> */}
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
